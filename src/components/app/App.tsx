/**
 * @file Top level application component.
 */


import React from 'react'

import {
  createMuiTheme,
  responsiveFontSizes,
  ThemeProvider
} from '@material-ui/core/styles'

import Login from '../login/Login'
import './app.scss'


/* Material UI theme and typography */
const theme = responsiveFontSizes(
  createMuiTheme(
    {
      palette: {
        primary: {
          main: '#83D300',
          contrastText: '#FFF',
        },
        secondary: {
          main: '#1B5E20',
        },
      },
      typography: {
        button: {
          fontWeight: 900,
          fontSize: '1.5em',
        }
      }
    }
  ),
  { factor: 1.66 }
)

/* Application property and state types */
type AppProps = {}
type AppState = {
  authenticated: boolean
}


/**
 * Class representing the application.
 * @extends React.component
 */
class App extends React.Component <AppProps, AppState> {

  /**
   * Sets default App state.
   * @constructor
   * @see React.Component
   */
  constructor(props: object){
    super(props)
    this.state = {
      authenticated: false
    }
  }

  /**
   * Shows application content to authenticated users, and login form to
   * anonymous users.
   * @see React.Component.render
   */
  render() {
    return (
      <ThemeProvider theme={theme}>
        { this.state.authenticated
          ? <section className="app">
              <header className="header"></header>
              <main className="main"></main>
              <footer></footer>
            </section>
          : <Login></Login>
        }
      </ThemeProvider>
    )
  }
}


export default App