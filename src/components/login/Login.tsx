/**
 * @file Login component.
 */


import React from 'react'
import {Grid, Link} from '@material-ui/core'
import UsernameField from './UsernameField'
import PasswordField from './PasswordField'
import LoginButton from './LoginButton'
import logo from '../../images/logo.svg'
import './login.scss'


/* Login property and state types */
type LoginProps = {}
type LoginState = {
  username: string,
  password: string,
  showPassword: boolean
}


/**
 * Class representing user login.
 * @extends React.component
 */
class Login extends React.Component<LoginProps, LoginState> {

  /**
   * Set initial state, properties and bind event handlers.
   * @constructor
   * @param props
   * @see React.Component
   */
  constructor(props: object) {
    super(props)
    this.state = {
      username: '',
      password: '',
      showPassword: false
    }
  }

  /**
   * Render login form.
   * @see React.Component.render
   */
  render() {
    return (

      <Grid
        container
        className="login"
        alignItems="center"
        wrap="wrap"
      >

        <Grid
          item xs={12} sm={6}
          container justify="space-around">
          <Grid item xs={8}>
            <img src={logo} width="100%" className="logo" alt="logo" />
          </Grid>
        </Grid>

        <Grid
          item xs={12} sm={6}
          container justify="space-around">
          <Grid item xs={8}>
            <form className="login-form" autoComplete="off">

              <UsernameField></UsernameField>
              <PasswordField></PasswordField>
              <LoginButton></LoginButton>

              <Link className="create-account" href="#">Create an account</Link>

            </form>
          </Grid>
        </Grid>
      </Grid>
    )
  }
}


export default Login