/**
 * @file LoginButton component.
 */


import React from 'react'
import Button from '@material-ui/core/Button'
import LockOpen from '@material-ui/icons/LockOpen'


/* Property and State type definitions */
type Props = {
  onChange?: CallableFunction
}
type State = {}


/**
 * Class representing user login.
 * @extends React.component
 */
class LoginButton extends React.Component <Props, State> {

  /**
   * Rendered login button.
   * @see React.Component.render
   */
  render() {
    return (

      <Button
        className="input input-button"
        variant="contained"
        color="primary"
      >
        Log in
        <LockOpen className="icon"></LockOpen>
      </Button>

    )
  }
}


export default LoginButton