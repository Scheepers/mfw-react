/**
 * @file Password component.
 */


import React from 'react'
import {
  FormControl,
  OutlinedInput,
  IconButton,
  InputAdornment
} from '@material-ui/core'
import { Lock, Visibility, VisibilityOff} from '@material-ui/icons'


/* Property and State type definitions */
type Props = {
  password?: string,
  onChange?: CallableFunction
}
type State = { showPassword?: boolean }


/**
 * Class representing user login password field.
 * @extends React.component
 */
class Password extends React.Component <Props, State> {

  /**
   * Set initial state and properties.
   * @constructor
   * @param props
   * @see React.Component
   */
  constructor(props: object) {
    super(props)
    this.state = {
      showPassword: false
    }
  }

  /**
   * Shows login form with appropriate messaging.
   * @see React.Component.render
   */
  render() {
    return (

      <FormControl
        className="input field"
        variant="outlined"
        hiddenLabel
      >

        <OutlinedInput
            id="outlined-adornment-password"
            type={this.state.showPassword ? 'text' : 'password'}
            value={this.props.password}
            placeholder="Password"
            onChange={
              (event) => this.props.onChange && this.props.onChange(event)
            }
            startAdornment={
            <InputAdornment position="start">
                <Lock color="disabled"></Lock>
            </InputAdornment>
            }
            endAdornment={
            <InputAdornment position="end">
                <IconButton
                aria-label="toggle password visibility"
                onClick={() => this.togglePassword()}
                onMouseDown={(e) => e.preventDefault()}
                edge="end"
                >
                {this.state.showPassword
                    ? <Visibility />
                    : <VisibilityOff />
                }
                </IconButton>
            </InputAdornment>
            }
        />
      </FormControl>
    )
  }

  togglePassword() {
    this.setState({ showPassword: !this.state.showPassword })
  }
}


export default Password