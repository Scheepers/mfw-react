/**
 * @file UserNameField component.
 */


import React from 'react'
import {
  TextField,
  InputAdornment
} from '@material-ui/core'
import AccountCircle from '@material-ui/icons/AccountCircle'


/* Property and State type definitions */
type Props = {
  username?: string,
  onChange?: CallableFunction
}
type State = {}


/**
 * Class representing user name field.
 * @extends React.component
 */
class UserNameField extends React.Component <Props, State> {

  /**
   * Shows login form with appropriate messaging.
   * @see React.Component.render
   */
  render() {
    return (

      <TextField
        className="input field"
        placeholder="Username"
        variant="outlined"
        value={this.props.username}
        onChange={
          (event) => this.props.onChange && this.props.onChange(event)
        }
        InputProps={
          {
            startAdornment:
              <InputAdornment position="start">
                <AccountCircle color="disabled"></AccountCircle>
              </InputAdornment>,
          }
        }
      />

    )
  }
}


export default UserNameField